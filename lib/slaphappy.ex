defmodule Slaphappy do
  @moduledoc """
  Documentation for Slaphappy.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Slaphappy.hello
      :world

  """
  def hello do
    :world
  end
end
