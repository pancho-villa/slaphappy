defmodule Slaphappy.Sensor do
  use GenServer
  require Logger

  def start_link(_) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def player_playing() do
    GenServer.call(__MODULE__, :player_playing)
  end

  def init(_) do
    Logger.debug("not playing")
    send(self(), :query_player)
    {:ok, false}
  end

  def handle_info(:query_player, _) do
    #write logic here to handle the http query reading the state of kodi
    #for now handle a non-playing player
    player_state = false
    {:noreply, player_state}
  end

  def handle_call(:player_playing, _, state) do
    Logger.debug(state)
    {:reply, false}
  end

end