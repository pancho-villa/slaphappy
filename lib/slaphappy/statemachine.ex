defmodule Slaphappy.StateMachine do
  @moduledoc """
  <pre>
              ┌────────────────Giving up────────────┐┌──Retry──┐
              │                                     ││         │
              ▼                                     │▼         │
     ┌────────────────┐                    ┌────────────────┐  │
     │                │                    │                │  │
     │      Idle      │◀────show launched──│  Launch show   │──┘
     │                │◀─┐                 │                │
     └────────────────┘  │                 └────────────────┘
              │          │                          ▲
              │          │                          │
              │          │                          │
        Button pressed   │                          │
              │          │                          │
              ▼          │                          │
     ┌────────────────┐  │                          │
     │                │  │                          │
     │    Check       │  │                          │
  ┌──│    Player      │──────Player not playing─────┘
  │  │                │  │
  │  └────────────────┘  │
  │                      │
  └──────────────────────┘
      Player currently
          playing
            
  </pre>
  """

  use GenServer
  require Logger

  def start_link(_) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def button_pressed() do
    GenServer.cast(__MODULE__, :button_pressed)
  end

  @doc """
  Possible states:
  - :idle
  - :checking_player
  - :playing
  """
  def init(_) do
    {:ok, %{status: :idle}}
  end

  def player_playing() do
    GenServer.cast(__MODULE__, :player_playing)
  end

  def handle_cast(:button_pressed, %{status: :idle} = state) do
    Logger.debug("BUTTON PRESSED")
    Slaphappy.Switch.lights_off()
    {:noreply, %{state | status: :checking_player}}
  end

  def handle_cast(_msg, state) do
    {:noreply, state}
  end

  def handle_call(:player_playing) do
    #put in the logic here to check against the pinche server
    {:ok, %{status: :playing}}
  end

  def check_player() do
    Logger.debug("Checking player inside the statemachine.")
    if player_playing() == %{status: :playing} do
      Logger.debug("got false for the player, now gonna do a thingie.")
      #send(self(), :light_show)
      new_state = %{status: :playing}
      {:noreply, new_state}
    else
      Logger.info("Kodi is already playing, resetting back to idle.")
      {:noreply, %{status: :idle}}
    end
  end

#  def handle_cast(:light_show) do
#    {:noreply, %{status: :playing}}
#  end

end