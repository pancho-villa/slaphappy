defmodule Slaphappy.Switch do
  use GenServer
  require Logger
  
  @gpio_pin 17
  @blue 26
  @green 25
  @yellow 24
  @white 23
  @pins [@blue, @green, @yellow, @white]

  def start_link(_) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_) do
    {:ok, gpio} = Circuits.GPIO.open(@gpio_pin, :input, pull_mode: :pullup)
    Circuits.GPIO.set_interrupts(gpio, :both)
    lights_on()
    {:ok, %{gpio: gpio}}
  end

  def handle_info({:circuits_gpio, 17, _timestamp, 0}, state) do
    Logger.debug("The circuits_gpio emitted button press with: ")
#    Logger.debug(state)
    Logger.debug("Caught a button press!")
    Slaphappy.StateMachine.button_pressed()
    lights_off()
    {:noreply, state}
  end

  def handle_info({:circuits_gpio, 17, _timestamp, 1}, state) do
    Logger.debug("The circuits_gpio released button with: ")
    Logger.debug("Button released!")
    lights_on()
    {:noreply, state}    
  end

  def handle_info(_msg, state) do
    Logger.debug("Something definitely happened here.")
    {:noreply, state}
  end

  def trill_lights() do
    Logger.debug("now trilling.")
    sleep_interval = 720
    for p <- @pins do
      {:ok, pin} = Circuits.GPIO.open(p, :output)
      Circuits.GPIO.write(pin, 1)
      Circuits.GPIO.close(pin)
      Process.sleep(sleep_interval/2)
    end
  end

  def lights_on() do
    Logger.debug("turning on the lights")
    for p <- @pins do
      {:ok, pin} = Circuits.GPIO.open(p, :output)
      Circuits.GPIO.write(pin, 1)
      Circuits.GPIO.close(pin)
    end
  end

  def lights_off() do
    Logger.debug("turning off the lights")
    for p <- @pins do
      {:ok, pin} = Circuits.GPIO.open(p, :output)
      Circuits.GPIO.write(pin, 0)
      Circuits.GPIO.close(pin)
    end
  end

end
