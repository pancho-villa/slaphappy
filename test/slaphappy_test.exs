defmodule SlaphappyTest do
  use ExUnit.Case
  doctest Slaphappy

  test "greets the world" do
    assert Slaphappy.hello() == :world
  end

  test "ping" do
    GenServer.call(Slaphappy.StateMachine, :ping)
  end
end
