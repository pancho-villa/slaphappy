release :slaphappy do
  set version: current_version(:slaphappy)
  plugin Shoehorn
end